<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function(Blueprint $table) {
			$table->mediumInteger('id', true, true);
			$table->smallInteger('user_type_id', false, true)
				->default(Config::get('keys.UserTypeCustomerId'))
				->references('id')->on('user_types');
			$table->string('forename', 24);
			$table->string('surname', 24)->nullable();
			$table->string('email_address', 180)->unique();
			$table->string('password', 60);
			$table->rememberToken();
			$table->timestamps();
		});
		
		Schema::create('user_types', function(Blueprint $table) {
			$table->smallInteger('id', true, true);
			$table->string('name');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users');
		Schema::drop('user_types');
    }
}
