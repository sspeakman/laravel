<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('user_types')->truncate();
        DB::table('user_types')->insert([
			'name' => 'Customer', 
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);
		DB::table('user_types')->insert([
			'name' => 'Administrator', 
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);
    }
}
