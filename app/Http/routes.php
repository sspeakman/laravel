<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Specify the homepage
Route::get('/', 'PagesController@display');

// Create a custom pages route
Route::get('pages/{page}', ['uses' => 'PagesController@display']);

// Lets test some stuff
Route::get('users', ['uses' => 'UsersController@index']);