<?php
namespace App\Http\Controllers;

class PagesController extends Controller {
	
	/**
	 * Simple method to display pages dynamically
	 * 
	 * @param string $page The view to render (default: home)
	 * @return type
	 */
	public function display($page = 'welcome') {
		return view($page);
	}
}