<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use App\Models\BaseModel;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['forename', 'surname', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
	
	/**
	 * Extend construct to automatically relate to UserType
	 * @ignore I am used to being able to specify relationships as a property of the model, setting them up each time 
	 * @ignore seems like it could be quite a pain. However, it's possible that setting up the relationship each
	 * @ignore time is actually going to cause more strain on the application. If this is the case then this should
	 * @ignore be changed.
	 * 
	 * @param array $attributes
	 */
	public function __construct(array $attributes = []) {
		parent::__construct($attributes);
		
		$this->belongsTo('\App\Models\UserType');
	}
}
