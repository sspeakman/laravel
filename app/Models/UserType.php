<?php

namespace App\Models;
use App\Models\BaseModel;

class UserType extends BaseModel {
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_types';

	/**
	 * Extend construct to automatically relate to User
	 * @ignore I am used to being able to specify relationships as a property of the model, setting them up each time 
	 * @ignore seems like it could be quite a pain. However, it's possible that setting up the relationship each
	 * @ignore time is actually going to cause more strain on the application. If this is the case then this should
	 * @ignore be changed.
	 * 
	 * @param array $attributes
	 */
	public function __construct(array $attributes = []) {
		parent::__construct($attributes);
		
		$this->belongsTo('\App\Models\User');
	}
	
}
