<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
	
	/**
	 * Simple method to find a single row by primary key (testing base model extension)
	 * 
	 * @param int $primaryKey The primary key to search by
	 * @param string $relationship The table to query if not the model's own table
	 * @return type
	 */
	public static function findById($primaryKey = null, $relationship = null) {
		if(! $relationship) {
			$relationship = $this->table;
		}
		
		$records = DB::table($relationship)->where('id', $primaryKey)->first();
		
		return $records;
	}
	
}
