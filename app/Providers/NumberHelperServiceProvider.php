<?php
namespace app\Providers;
use Illuminate\Support\ServiceProvider;

class NumberHelperServiceProvider extends ServiceProvider {
	
	/**
	 * Register the service provider.
	 * 
	 * @return void
	 */
	public function register() {
		$this->app['numberhelper'] = $this->app->share(function($app) {
			return new \App\Helpers\NumberHelper;
		});
	}
}