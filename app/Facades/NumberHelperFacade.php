<?php
namespace app\Facades\Helpers;
use Illuminate\Support\Facades\Facade;

class NumberHelperFacade extends Facade {
	
	/**
	 * Get the registered name of the helper.
	 * 
	 * @return string The helper
	 */
	protected static function getFacadeAccessor() {
		return "numberhelper";
	}
}