<?php
namespace App\Helpers;

use App\Helpers\Helper;

class NumberHelper extends Helper {

	/**
	 * Format a number into a currency with a few options
	 * 
	 * @param float|int $value The number to convert
	 * @param string $symbol The symbol to add if applicable
	 * @param array $options An array of options to send
	 * @return string The formatted number currency
	 */
	public function currency($value, $symbol = null, array $options = []) {
		$value = (float)$value;

		if(isset($options['zero']) && !$value) {
			return $options['zero'];
		}

		$before = isset($options['before']) ? $options['before'] : null;
		$after = isset($options['after']) ? $options['after'] : null;
		$decimals = isset($options['decimals']) ? $options['decimals'] : 2;
		return $before . $symbol . number_format($value, $decimals) . $after;
	}
	
}